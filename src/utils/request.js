import axios from 'axios'
import store from 'store'
import { Message } from 'element-ui'
// Calculating...

const service = axios.create({
  baseURL: 'http://58.87.89.197:8360/api/private/v1/', // 请求根路径
  timeout: 30000 // request timeout
})

// request interceptor 发起请求之前
service.interceptors.request.use(
  config => {
    if (store.state.token) {
      config.headers.Authorization = store.state.token // 在每次发请求之前，给这次的请求头里的 Authorization 字段 加一个 token
    }
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

// response interceptor 发起请求成功之后
service.interceptors.response.use(
  res => {
    // console.log(res)
    const {
      data
    } = res
    if (data.meta.status === 200 || data.meta.status === 201) {
      return data
    }
  },
  error => {
    return Promise.reject(error)
  }
)

export default service