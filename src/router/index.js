import Vue from 'vue'
import VueRouter from 'vue-router'
// import { component } from 'vue/types/umd'
import Store from '../store'
import Login from 'views/login/index.vue'
import Menus from 'components/Menus/index.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    component: Login
  },
  {
    path: '/',
    redirect: '/Menus',
  },
  {
    path: '/menus',
    component: Menus,
  },
  {
    path: '/orders',
    component: () => import('views/Order')
  },
  {
    path: '/users',
    component: () => import('views/UserList')
  },
  {
    path: '/rights',
    component: () => import('views/power/Rights')
  },
  {
    path: '/roles',
    component: () => import('views/power/Roles')
  },
  {
    path: '/categories',
    component: () => import('views/goods/Cate')
  },
  {
    path: '/params',
    component: () => import('views/goods/Params')
  },
  {
    path: '/goods',
    component: () => import('views/goods/GoodsList')
  },
  {
    path: '/notFinished',
    component: () => import('views/NotFinished')
  },
  {
    path: '/details',
    component: () => import('views/Details')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// 路由守卫
router.beforeEach((to, from, next) => {
  if (to.name !== 'Login' && !Store.state.token) next({ name: 'Login' })
  else next()
  // if (to.name === 'Login' && Store.state.token) next('/')
  // else next()
})

export default router
