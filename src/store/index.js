import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: localStorage.token || '',
    user: localStorage.user ? JSON.parse(localStorage.user) : {}
  },
  mutations: {
    SET_TOKEN: (state, token) => {
      localStorage.token = token
      state.token = token
    },
    SET_USER_INFO: (state, user) => {
      localStorage.user = JSON.stringify(user) // 转成字符串存到本地
      state.user = user
    }
  },
  actions: {
  },
  modules: {
  }
})
