'use strict'

const path = require('path')

function resolve(dir) {
  return path.join(__dirname, dir)
}
module.exports = {
  chainWebpack: config => {
    config.plugins.delete('named-chunks')
    config.resolve.alias.set('@', resolve('src'))
    config.resolve.alias.set('store', resolve('src/store'))
    config.resolve.alias.set('views', resolve('src/views'))
    config.resolve.alias.set('components', resolve('src/components'))
  },
  productionSourceMap: false,
  devServer: {
    port: 3000,
    open: true // 跑起来后直接打开网页
  }
}
